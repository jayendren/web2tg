# Server
require_relative '../etc/config.rb'
require 'sinatra'
require 'sinatra/namespace'
#require 'slim'
#set :views, Proc.new { File.join(root, "../templates") 

# Endpoints
get '/' do
  'nothing useful here...'.to_json
end

# Models 
require_relative '../models/web2tg.rb'

# Views
require_relative '../views/web2tg.rb'

# Controllers
require_relative '../controllers/web2tg.rb'

# Jobs
#require_relative '../jobs/poll_sent_state.rb'
#poll_sent_msgs = Pollmsgq.new
# load "#{File.dirname(__FILE__)}/../jobs/poll_sent_state.rb"
#require_relative '../jobs/poll_sent_state.rb'
#poll_sent_msgs = Pollmsgq.new