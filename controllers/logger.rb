require 'logger'

class Logg
  def self.log(
    log,
    msg,
    log_size = 1024000)

    logger      = Logger.new(log, log_size)
    if logger.nil?
      logger                 = Logger.new STDOUT
      logger.level           = Logger::DEBUG
      logger.datetime_format = '%Y-%m-%d %H:%M:%S '
    end
    logger.info "#{msg}"
  end
end