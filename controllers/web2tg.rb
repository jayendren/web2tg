# WEB2TG Library
require_relative '../models/web2tg.rb'

# Serializers
class Web2tgSerializer

  def initialize(web2tg)
    @web2tg = web2tg
  end

  def as_json(*)   
    data = {
      timestamp:  @web2tg.timestamp,
      messageid:  @web2tg.messageid,
      website:    @web2tg.website,
      txmsg:      @web2tg.txmsg,
      rxmsg:      @web2tg.rxmsg,
      state:      @web2tg.state,
      sessionid:  @web2tg.sessionid,
      user:       @web2tg.user,
    }
    data[:errors] = @web2tg.errors if @web2tg.errors.any?
    data
  end

end

class Web2tg

  def self.host_accessible?(
    host="api.telegram.org",
    port=443,
    timeout=1,
    sleep_period=1
    )
    begin
      Timeout::timeout(timeout) do
        begin
          s = TCPSocket.new(host, port)
          s.close
          return true
        rescue Errno::ECONNREFUSED, Errno::EHOSTUNREACH
          sleep(sleep_period)
          retry
        end
      end
    rescue Timeout::Error
      return false
    end
  end

  def self.host_lookup(host_or_ip)
    return Socket::getaddrinfo(host_or_ip, 'www', nil, Socket::SOCK_STREAM)[0][3]
  end


  def self.get(
    url,
    method     = "getUpdates",
    message    = nil,
    proxy_addr = nil, 
    proxy_port = nil,
    username   = nil,
    pass       = nil,
    timeout    = $tg_timeout        
    )

    if message.nil?
      uri              = URI.parse("#{url}/#{method}")
    else
      uri              = URI.parse("#{url}/#{method}#{message}")
    end
    #puts uri.to_json
    if proxy_addr && proxy_port
      http             = Net::HTTP.new(uri.host, uri.port, proxy_addr, proxy_port)
    else    
      http             = Net::HTTP.new(uri.host, uri.port,)
    end

    http.use_ssl       = (uri.scheme == "https")
    http.verify_mode   = OpenSSL::SSL::VERIFY_NONE
    http.read_timeout  = timeout           
    request            = Net::HTTP::Get.new(uri.request_uri)
    request.initialize_http_header({"Accept-Encoding" => "application/json"})

    if username && pass
      request.basic_auth(username,pass)
    end

    begin
      response                = http.request(request)
      if response.code == "200"
        result = 1
        rcode  = response.code + " passed"
      elsif response.code == "302"
        result = 1
        rcode  = response.code + " passed"
      else
        result = 0
        rcode  = response.code + " failed"
      end
      rescue Timeout::Error
        result = 0
        rcode  = "Timeout::Error"
      rescue Errno::ETIMEDOUT
        result = 0
        rcode  = "Errno::ETIMEDOUT"
      rescue Errno::EHOSTUNREACH
        result = 0
        rcode  = "Errno::EHOSTUNREACH"
      rescue Errno::ECONNREFUSED
        result = 0
        rcode  = "Errno::ECONNREFUSED"
      rescue SocketError => e
        result = 0
        rcode  = "SocketError"
      #rescue Zlib::BufError => e
      #  rcode  = e
      #  puts e    
    end
    return JSON.parse(response.body)
    http.finish
    
  end

  def self.getupdates(messageid)
    timestamp  = Time.new.strftime("%Y-%m-%dT%H:%M:%S")
    tg_result = Web2tg.get(
      $tg_url,
      "getUpdates"
    )

    tg_updates      = tg_result.fetch("result")
    web2tg          = Web2tgdb.all
    for index in 0...tg_updates.size
      message_id    = tg_result.fetch("result")[index].fetch("message").fetch("message_id")
      text          = tg_result.fetch("result")[index].fetch("message").fetch("text")
      reply_msgid   = (message_id.to_i - 1)
      puts "#{timestamp} checking #{messageid} matches #{reply_msgid} result from getUpdates"
      if messageid.to_i == reply_msgid
        @result =  "#{timestamp} #{messageid} match in getUpdates: #{text}"
        web2tg.where(messageid: messageid).
          update(state: "replied", 
            rxmsg: text
        )
        return @result
      end
    end

  end

  def self.msgq(state="sent")
    @pending_msgs = []
    web2tg        = Web2tgdb.all
    web2tg.each {|result|
      msgid    =  result[:messageid]
      msgstate =  result[:state]
      if msgstate.to_s.match("#{state}")
        @pending_msgs << msgid
      end
    }
    @pending_msgs.each {|msg|
      return Web2tg.getupdates(msg).to_json 
    }
  end

  def self.sendmsg(
    msg_arrhash,
    timeout  = 10,
    token    = $tg_api_token
    )
    @results  = []
    @result   = {}
    msg_arrhash.each {|msg|      
      website    = msg.fetch("website")
      txmsg      = msg.fetch("txmsg")
      rxmsg      = nil
      state      = "new"
      sessionid  = msg.fetch("sessionid")
      user       = msg.fetch("user")
      chatid     = msg.fetch("chatid")
      timestamp  = Time.new.strftime("%Y-%m-%dT%H:%M:%S")

      if Web2tg.host_accessible?
          tg_meta = URI.encode("#{user}: #{website}): #{txmsg}")
          tg_result = Web2tg.get(
            $tg_url,
            "sendMessage?chat_id=#{chatid}&text=",
            "#{tg_meta}"
          )

          @messageid  =  tg_result['result'].fetch("message_id")
          if @messageid
            messageid = @messageid
            @state    = "sent"            
          else
            messageid = SecureRandom.hex
            @state    = "failed"
          end

      else
         @state = "failed"
      end

      @result = Web2tgdb.create(
        timestamp:  timestamp,
        messageid:  messageid,
        website:    website,
        txmsg:      txmsg,
        rxmsg:      rxmsg,
        state:      @state,
        sessionid:  sessionid,
        user:       user,
        chatid:     chatid,
      )
      @results << @result

    }
    return @results
  end
end

