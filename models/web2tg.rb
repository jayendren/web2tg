# WEB2TG MODEL
require_relative '../etc/config.rb'

# DB Config
Mongoid.load! "#{File.dirname(__FILE__)}/web2tg.yaml"

# Models
class Web2tgdb
  include Mongoid::Document

  field :timestamp,   type: String
  field :messageid,   type: String
  field :website,     type: String
  field :txmsg,       type: String
  field :rxmsg,       type: String
  field :state,       type: String
  field :sessionid,   type: String
  field :user,        type: String
  field :chatid,      type: String

  validates :timestamp,  presence: true
  validates :messageid,  presence: true
  validates :website,    presence: true
  validates :txmsg,      presence: true
  #validates :rxmsg,      presence: true
  validates :state,      presence: true
  validates :sessionid,  presence: true
  validates :user,       presence: true
  validates :chatid,     presence: true

  index({ messageid: 'text' })

  scope :id,        -> (id)        { where(id:        id) }
  scope :messageid, -> (messageid) { where(messageid: messageid) }
  scope :website,   -> (website)   { where(website:   /^#{website}/) }
  scope :txmsg,     -> (txmsg)     { where(txmsg:     /^#{txmsg}/) } 
  scope :rxmsg,     -> (rxmsg)     { where(rxmsg:     /^#{rxmsg}/) } 
  scope :state,     -> (state)     { where(state:     /^#{state}/) } 
  scope :sessionid, -> (sessionid) { where(sessionid: sessionid) }
  scope :user,      -> (user)      { where(user:      user) }
end