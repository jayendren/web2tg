* [Getting started](#Getting-started)
* [Requirements](#requirements)
* [API](#api)
* [Author](#author)

Getting started
=========


About
------------

- sinatra/mongodb/rufus-scheduler based web2tg api for allowing websites with chat integration frames/windows to send messages to telegram.

  - NB: early ALPHA release !
    - no api authentication has been implemented
    - no ssl support


```
    ###############     ############    ##############     ############
    #   WWW_CHAT  # ->  #   REST   # -> # web2tg_api # ->  # TELEGRAM #
    ###############     ############    ##############     ############
           ^                 ^                  ^                |
           |_________________|____getUpdates____|_______________<|
           |                                                     ^
           |                                                     |
     ###############                                        ###########
     # WWW_VISITOR #                                        # TG_USER #
     ###############                                        ###########

```


Requirements
------------

- sessionid

``` 
unique_string for client side user session
```

- user 

```
user@tld (retrieved from sessionid of user sending message)
```

- chatid 

```
telegram chatid of user responding to txmsg
```

- txmsg

```
message generated from chat_screen
```

- website

```
website name sending message to TELEGRAM
```

API
--------------

**Send Message**
----
  Send message to telegram user

* **URL**

  api/v1/web2tg

* **Method:**
  

  `POST`


*  **URL Params**
  

   **Required:**
 
   `Content-Type: application/json`
   `sessionid=[user websessionid]`
   `user=[user@tld]`
   `txtmsg=[string]`
   `website=[originating website]`
   `chatid=[telegram user id answering]`


   **Optional:**
 
   `state=[new]`

* **Data Params**

  ```
  [
  {"sessionid":"99fdbbe3e5fd20af788c91c4ce679c61",
  "user":"john@doe.net",
  "txmsg":"I require 10x of itemx by next Friday, are you available? thanks.",
  "website":"test.dom",
  "chatid":"52061074"}
  ]
  ```

* **Success Response:**
  

  * **Code:** 200 <br />
 
* **Error Response:**

  * **Code:** 401 UNAUTHORIZED <br />
    **Content:** `{ error : "Log in" }`

  OR

  * **Code:** 422 UNPROCESSABLE ENTRY <br />
    **Content:** `{ error : "KeyError: key not found: "txmsg"" }`

* **Sample Call:**

```
curl -i -X POST -H "Content-Type: application/json" -d \
  '[{"sessionid":"99fdbbe3e5fd20af788c91c4ce679c61",
  "user":"john@doe.net",
  "txmsg":"I require 10x of itemx by next Friday, are you available? thanks.",
  "website":"test.dom",
  "chatid":"52061074"}]' \
  http://localhost:4567/api/v1/web2tg
HTTP/1.1 200 OK
Content-Type: application/json
Location: http://localhost:4567/api/v1/web2tg/622
Content-Length: 0
X-Content-Type-Options: nosniff
Server: WEBrick/1.3.1 (Ruby/2.1.5/2014-11-13)
Date: Sat, 03 Sep 2016 01:02:36 GMT
Connection: Keep-Alive
```

* **Notes:**

  None



**Get Message**
----
  Get message

* **URL**

  api/v1/web2tg/messageid
  api/v1/webt2g?messageid=$messageid

* **Method:**
  

  `GET`


*  **URL Params**
  

   **Required:**
 
   `Content-Type: application/json`
   `messagid=[messageid]`


   **Optional:**
 
   None

* **Data Params**

  None

* **Success Response:**
  

  * **Code:** 200 <br />
 

* **Sample Call:**

```
curl -Ss 'http://localhost:4567/api/v1/web2tg?messageid=628' | jq .
[
  {
    "timestamp": "2016-09-03T10:35:38",
    "messageid": "628",
    "website": "test.dom",
    "txmsg": "I require 10x of itemx by next Friday, are you available? thanks.",
    "rxmsg": "sure",
    "state": "replied",
    "sessionid": "99fdbbe3e5fd20af788c91c4ce679c61",
    "user": "john@doe.net"
  }
]


curl -Ss 'http://localhost:4567/api/v1/web2tg/628' | jq .
{
  "timestamp": "2016-09-03T10:35:38",
  "messageid": "628",
  "website": "test.dom",
  "txmsg": "I require 10x of itemx by next Friday, are you available? thanks.",
  "rxmsg": "sure",
  "state": "replied",
  "sessionid": "99fdbbe3e5fd20af788c91c4ce679c61",
  "user": "john@doe.net"
}
```

* **Notes:**

  None


**Delete Message**
----
  Delete message from database

* **URL**

  api/v1/web2tg/messageid

* **Method:**
  

  `DELETE`


*  **URL Params**
  

   **Required:**
 
   `Content-Type: application/json`
   `messageid=[messageid]`


   **Optional:**
 
   None

* **Data Params**

  ```
  [
  {"messageid":"messageid",}
  ]
  ```

* **Success Response:**
  

  * **Code:** 204 <br />
 
* **Error Response:**


* **Sample Call:**

```
curl -i -X DELETE -H "Content-Type: application/json" \
http://localhost:4567/api/v1/web2tg/628
HTTP/1.1 204 No Content
X-Content-Type-Options: nosniff
Server: WEBrick/1.3.1 (Ruby/2.1.5/2014-11-13)
Date: Sat, 03 Sep 2016 08:53:47 GMT
Connection: Keep-Alive
```

* **Notes:**

  WWW_CHAT should delete state=replied message after successful display.


Author
------------

- Jay Maduray <jayendren@gmail.com>
