require 'rufus/scheduler'
require 'sinatra'
require_relative '../controllers/logger.rb'
require_relative '../controllers/web2tg.rb'

class Pollmsgq < Sinatra::Base
  configure do
    set :threaded, false
  end    
    #EM.defer do

  log_dir   = $job_logdir
    scheduler = Rufus::Scheduler::singleton
    scheduler.every '5s' do
        poll_sent_state = Web2tg.msgq(state="sent")
        #timestamp = Time.new.strftime("%Y-%m-%dT%H:%M:%S")
        unless poll_sent_state.empty?
          scheduler_handler = scheduler.inspect.to_s.strip
          Logg.log("#{log_dir}/Pollmsgq.log",
            "#{scheduler_handler} : "
            )
        end
    end
    scheduler.join

    #end
    #puts 'I\'m doing work in the background, but I am still free to take requests'


end