# WEB2TG API
require 'sinatra'
require 'sinatra/namespace'

# Endpoints
get '/web2tg' do
  web2tg = Web2tgdb.all

  [:messageid, :website, :txmsg, :rxmsg, :state, :sessionid, :user].each do |filter|
    web2tg = web2tg.send(filter, params[filter]) if params[filter]
  end

  web2tg.to_json
end

# Namespaces
namespace '/api/v1' do

  before do
    content_type 'application/json'
  end

  helpers do
    def base_url
      @base_url ||= "#{request.env['rack.url_scheme']}://#{request.env['HTTP_HOST']}"
    end

    def json_params
      begin
        JSON.parse(request.body.read)
      rescue
        halt 400, { message: 'Invalid JSON' }.to_json
      end
    end

    # Using a method to access the src_ip can save us
    # from a lot of repetitions and can be used
    # anywhere in the endpoints during the same
    # request
    def web2tg
      @web2tg ||= Web2tgdb.where(messageid: params[:messageid]).first
    end

    # Since we used this code in both show and update
    # extracting it to a method make it easier and
    # less redundant
    def halt_if_not_found!
      halt(404, { message: 'Sorry... Not Found'}.to_json) unless web2tg
    end

    def serialize(web2tg)
      Web2tgSerializer.new(web2tg).to_json
    end
  end

  ## READ
  # curl -Ss http://localhost:4567/web2tg  | jq .
  # [
  #   {
  #     "_id": {
  #       "$oid": "57c02007f55195aedb2b66cc"
  #     },
  #     "website": "www.mysite.com",
  #     "messageid": "3b1c93a211e2d6c1c0f2c4537a037ea6",
  #     "sessionid": "99fdbbe3e5fd20af788c91c4ce679c61",
  #     "state": "new",
  #     "user": "john@doe.net",
  #     "message": "I require 10x of itemx by next Friday, can you help?",
  #     "timestamp": "2016-08-26T12:55:02"
  #   }
  # ]
  #
  ##  FILTERING 
  # curl -Ss 'http://localhost:4567/web2tg?state=replied'  | jq .
  # [
  #   {
  #     "_id": {
  #       "$oid": "57c0214ef55195b2b5825413"
  #     },
  #     "website": "www.mysite.com",
  #     "messageid": "3b1c93a211e2d6c1c0f2c4537a037ea6",
  #     "sessionid": "99fdbbe3e5fd20af788c91c4ce679c61",
  #     "state": "replied",
  #     "user": "john@doe.net",
  #     "message": "I require 10x of itemx by next Friday, can you help?",
  #     "timestamp": "2016-08-26T12:55:02"
  #   }
  # ]  
  get '/web2tg' do
    web2tg = Web2tgdb.all

    [:messageid, :website, :txmsg, :rxmsg, :state, :sessionid, :user].each do |filter|
      web2tg = web2tg.send(filter, params[filter]) if params[filter]
    end

    web2tg.map { |web2tg| Web2tgSerializer.new(web2tg) }.to_json
  end

  get '/web2tg/:messageid' do |messageid|    
    halt_if_not_found!    
    #unless id.nil? 
      #Web2tg.getupdates(messageid).to_json 
    #end
    #sleep 5
    serialize(web2tg)
  end

  ## CREATE ##
  # curl -i -X POST -H "Content-Type: application/json" -d \
  # '[{"sessionid":"99fdbbe3e5fd20af788c91c4ce679c61","state":"new","user":"john@doe.net","message":"I%20require%2020x%20of%20itemX%20by%20friday","website":"www.mysite.com"}]' \
  # http://localhost:4567/api/v1/web2tg \
  # && echo
  # HTTP/1.1 200 OK
  # Content-Type: application/json
  # Location: http://localhost:4567/api/v1/web2tg/1
  # Content-Length: 69
  # X-Content-Type-Options: nosniff
  # Server: WEBrick/1.3.1 (Ruby/2.1.5/2014-11-13)
  # Date: Wed, 24 Aug 2016 23:13:20 GMT
  # Connection: Keep-Alive
  # 

  # We switched from an if...else statement
  # to using a guard clause which is much easier
  # to read and makes the flow more logical
  post '/web2tg' do
    web2tg = Web2tg.sendmsg(json_params)
    web2tg.each {|msg|
      @messageid = msg[:messageid]
    }    
    response.headers['Location'] = "#{base_url}/api/v1/web2tg/#{@messageid}"
    #response.headers['Result']   = "#{web2tg}"
    halt 422, serialize(web2tg) unless web2tg    
  end

  ## UPDATE ##
  # curl -i -X PATCH -H "Content-Type: application/json" -d \
  # '{"state":"completed"}' \
  # http://localhost:4567/api/v1/web2tg/1
  # HTTP/1.1 200 OK
  # Content-Type: application/json
  # Content-Length: 247
  # X-Content-Type-Options: nosniff
  # Server: WEBrick/1.3.1 (Ruby/2.1.5/2014-11-13)
  # Date: Fri, 26 Aug 2016 11:05:19 GMT
  # Connection: Keep-Alive
  # 

  # Just like for the create endpoint,
  # we switched to a guard clause style to
  # check if the messageid is not found or if
  # the data is not valid
  patch '/web2tg/:messageid' do |id|
    halt_if_not_found!
    halt 422, serialize(web2tg) unless web2tg.update_attributes(json_params)
    serialize(web2tg)
  end

  ## DELETE ##
  # curl -i -X DELETE -H "Content-Type: application/json" \
  # http://localhost:4567/api/v1/web2tg/2e829631aa5b9fa84f630e746ace4a5d
  # HTTP/1.1 204 No Content
  # X-Content-Type-Options: nosniff
  # Server: WEBrick/1.3.1 (Ruby/2.1.5/2014-11-13)
  # Date: Fri, 26 Aug 2016 10:57:19 GMT
  # Connection: Keep-Alive  
  delete '/web2tg/:messageid' do |id|
    web2tg.destroy if web2tg
    status 204
  end
#### WEB2TG API ####

end 